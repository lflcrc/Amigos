-- execute under an account with the required privileges.


-- Si ejecuta este ejemplo, cambie inmediatamente la contraseña.
-- Use el commando password en sql*plus, o bien,
-- desde una sesión de administrador de base de datos use el comando
-- 'alter user amigos identified by x', donde x es su nueva contraseña

create user amigos2
    identified by insegura
    default   tablespace users
    temporary tablespace temp;


grant  connect, create session, alter session, resource,
       create synonym, create view
   to  amigos2; 

 
