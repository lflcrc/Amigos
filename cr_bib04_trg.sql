

CREATE SEQUENCE ENTIDADES_SQ INCREMENT BY 1 MINVALUE 200 CACHE 20;




  CREATE OR REPLACE TRIGGER "CONTACTOS_TRG01" 
before insert or delete or update on contactos
for each row 
begin
  if inserting then
      pq10.bitacora_usuarios(user || ' intenta inserción en contactos '
          || ' con entidad_id=' || :new.entidad_id || ' y id= '|| :new.id);
  elsif updating then
      pq10.bitacora_usuarios(user || ' intenta actualizar contactos');
  elsif deleting then
      pq10.bitacora_usuarios(user || ' intenta eliminar contactos');
  end if;
end;
/

ALTER TRIGGER "CONTACTOS_TRG01" ENABLE;
 

  CREATE OR REPLACE TRIGGER "ENTIDADES_TRG01" 
before insert or delete or update on entidades
for each row
begin
  if inserting then

       if :NEW."ID" is null then          
          select ENTIDADES_SQ.nextval 
            into :NEW."ID" 
            from dual;       
        end if;

      pq10.bitacora_usuarios(user || ' intenta inserción en entidades '
          || ' con id=' || :new.id || ' y nombre= '|| :new.nombre);

  elsif updating then

      pq10.bitacora_usuarios(user || ' intenta actualizar entidades');

  elsif deleting then

      pq10.bitacora_usuarios(user || ' intenta eliminar entidades');

  end if;
end;
/



ALTER TRIGGER "ENTIDADES_TRG01" ENABLE;
 
