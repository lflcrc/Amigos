CREATE OR REPLACE package pq10 as
   k_nom_paquete     CONSTANT VARCHAR2(20) := 'PQ10';
   k_fec_instanciado          VARCHAR2(20);
       
   -- devuelve la cantidad de formas registradas en el sistema
   -- para contactar a una persona.
   function num_contactos_persona (id entidades.id%type) return number;
   
   -- graba informaci�n de una persona y una manera de contactarla.
   procedure nueva_persona_y_contacto (
        p_nombre           entidades.nombre%type
      , p_fecha_nacimiento entidades.fecha_nacimiento%type
      , p_fecha_muerte     entidades.fecha_muerte%type
      , p_direccion        contactos.direccion%type
      , p_telefono         contactos.telefono%type
      , p_fecha_inicio     contactos.fecha_inicio%type
      , p_fecha_fin        contactos.fecha_fin%type
      );
      
    procedure provocar_excepcion;
    
    procedure bitacora_usuarios(x IN VARCHAR2);
    
    procedure probar;
    
    procedure limpiar;
    
end pq10;

/


CREATE OR REPLACE package body pq10 as

   -- declaraci�n de constantes, cursores, o procedimientos 
   -- privados del paquete.
--CLEAN   kk_nom_paquete      k_nom_paquete%type;
   kk_hora_instanciado k_fec_instanciado%type;

   
   
   cursor cantidad_contactos_por_entidad (p_id entidades.id%type) is
       select count(*) cantidad
         from contactos
        where entidad_id = p_id;
   
   
   
   procedure inserta_entidad(
   
       p_reg_entidad entidades%rowtype
     , p_entidad_id IN OUT entidades.id%type ) as
     
   begin

       insert into entidades (nombre, fecha_nacimiento)
            values (p_reg_entidad.nombre, p_reg_entidad.fecha_nacimiento)
            returning id into p_entidad_id;
   
   end inserta_entidad;
      
   
   
   
   procedure test_inserta_entidad(
 
       nom             entidades.nombre%type
     , fec             entidades.fecha_nacimiento%type
 
   ) is
 
       eid             entidades.id%type;
       reg_entidades   entidades%rowtype;
 
   begin
   
       dbms_output.put_line('Intentando insertar en entidades...');
       dbms_output.put_line(        'nombre =' || nom || 
                             ',' || 'fecha  =' || fec || 
                             '.');
                             
       reg_entidades.nombre           := nom;
       reg_entidades.fecha_nacimiento := fec;
   
       inserta_entidad (reg_entidades, eid);
   
       dbms_output.put_line('Devuelve id ='|| eid);
   
   exception WHEN others then
   
       rollback;
       dbms_output.put_line('Hubo una excepci�n: ' || sqlerrm(sqlcode));
       dbms_output.put_line('Se intentar� re-levantar la excepci�n...');
       raise_application_error(-20000, 'Excepci�n atrapada: ' 
                                       || sqlerrm(sqlcode));
       
   end test_inserta_entidad;
   
   
   
   procedure inserta_contacto(r contactos%rowtype ) as
      v_reg_contactos  contactos%rowtype := r;
   begin
       -- averigua cual debe ser el siguiente id de los contactos.
       select nvl( max(id) + 1 ,1) 
         into v_reg_contactos.id
         from contactos
        where entidad_id = r.entidad_id;

       insert into contactos  values v_reg_contactos;
   end inserta_contacto;
   
   
   
   procedure test_inserta_contacto(p_nombre_entidad varchar2) is
        v_reg_contacto   contactos%rowtype;
      
        -- funci�n declarada dentro de un procedimiento.
        -- No es visible fuera del procedimiento.
        function obtiene_id (n entidades.nombre%type) 
            return entidades.id%type is
            v_rslt entidades.id%type;
        begin
            select  id
              into  v_rslt
              from  entidades
             where  n = nombre;
            return  v_rslt;
        end obtiene_id;
            
   begin
        
        dbms_output.put_line('Intentado insertar un contacto...');
        
        v_reg_contacto.entidad_id   := obtiene_id(p_nombre_entidad);
        v_reg_contacto.id := NULL;  -- inserta_contacto proveer� este valor!
        v_reg_contacto.direccion    := 'direccion ' || p_nombre_entidad;
        v_reg_contacto.telefono     := substr('tel ' || p_nombre_entidad, 1,15);
        v_reg_contacto.fecha_inicio := to_date('2000/05/30','YYYY/MM/DD');
        v_reg_contacto.fecha_fin    := NULL;
                                                         
       dbms_output.put_line(      'Entidad_id='   || v_reg_contacto.entidad_id
                             || ', id='           || v_reg_contacto.id
                             || ', direccion='    || v_reg_contacto.direccion
                             || ', telefono='     || v_reg_contacto.telefono
                             || ', fecha_inicio'  || v_reg_contacto.fecha_inicio
                             || ', fecha_fin'     || v_reg_contacto.fecha_fin );                             
       inserta_contacto(v_reg_contacto);
   end test_inserta_contacto;
   
   
    
   function num_contactos_persona (id entidades.id%type) return number is
        cantidad pls_integer := 0;
   begin
        for r in cantidad_contactos_por_entidad(id) loop
            cantidad := r.cantidad;
        end loop;
        return cantidad;
   end  num_contactos_persona;
   
   
   
   procedure nueva_persona_y_contacto (
        p_nombre           entidades.nombre%type
      , p_fecha_nacimiento entidades.fecha_nacimiento%type
      , p_fecha_muerte     entidades.fecha_muerte%type
      , p_direccion        contactos.direccion%type
      , p_telefono         contactos.telefono%type
      , p_fecha_inicio     contactos.fecha_inicio%type
      , p_fecha_fin        contactos.fecha_fin%type
      ) 
   is
       k_nom_rutina constant  VARCHAR2(30) := 'nueva_persona_y_contacto';         
       v_reg_entidad    entidades%rowtype;
       v_reg_contacto   contactos%rowtype;
       v_entidad_id     entidades.id%type;
   
   begin
       v_reg_entidad.nombre           := p_nombre;
       v_reg_entidad.fecha_nacimiento := p_fecha_nacimiento;
       v_reg_entidad.fecha_muerte     := p_fecha_muerte;
       v_reg_contacto.direccion       := p_direccion;
       v_reg_contacto.telefono        := p_telefono;
       v_reg_contacto.fecha_inicio    := p_fecha_inicio;
       v_reg_contacto.fecha_fin       := p_fecha_fin;
       
       -- un bloque an�nimo
       begin
           savepoint restauracion_parcial;
           inserta_entidad(v_reg_entidad, v_entidad_id);
           v_reg_contacto.entidad_id   := v_entidad_id;
           inserta_contacto(v_reg_contacto);
       exception when others then
           rollback to restauracion_parcial;
           raise_application_error(-20000, 'Excepci�n en ' ||
             k_nom_paquete ||'.'|| k_nom_rutina ||':'|| sqlerrm(sqlcode));
       end;
       
   end nueva_persona_y_contacto;
  
  
  
  function division_entre_cero return pls_integer is
  begin
      return (1/0);
  end division_entre_cero;
  
  
  
   procedure provocar_excepcion is
      v_rslt PLS_INTEGER := 0;
   begin
      v_rslt := division_entre_cero;
   end provocar_excepcion;
   
   
   procedure dinamico is
   begin
      execute immediate 
          'create view direcciones_activas as '
       || 'select nombre, direccion, telefono '
       || 'from entidades e join contactos c '
       || '   on (e.id = c.entidad_id) '
       || 'where fecha_fin is null';
    end dinamico;
   
    
   
   procedure bitacora_usuarios(x IN VARCHAR2)
   is
      PRAGMA AUTONOMOUS_TRANSACTION;
   begin
      insert into bitacora (valor) values (x);
      commit;
   end bitacora_usuarios;
    
    

   procedure probar
   is 
   begin
        dbms_output.put_line('********');
        dbms_output.put_line('********');
        dbms_output.put_line('Probando 1,2,3... Probando...');
        
        dbms_output.put_line('*');
        dbms_output.put_line('Inserciones!');
        
        dbms_output.put_line('*');
        test_inserta_entidad('Albatros',  sysdate-365*20);
        test_inserta_contacto('Albatros');

        dbms_output.put_line('*');
        test_inserta_entidad('Buitre',    sysdate-365*19);
        test_inserta_contacto('Buitre');

        dbms_output.put_line('*');
        test_inserta_entidad('Cormor�n',  sysdate-365*18);
        test_inserta_contacto('Cormor�n');

        dbms_output.put_line('*');
        
        nueva_persona_y_contacto(
             'Dodo', sysdate-365*17, sysdate-365*3,
             'direccion del dodo', null, sysdate-365*17, sysdate-365*3
        );

        nueva_persona_y_contacto(
             'Faisan', sysdate-365*16, sysdate-365*2,
             'direccion del Faisan', null, sysdate-365*16, sysdate-365*2
        );

        nueva_persona_y_contacto(
             'Gaviota', sysdate-365*15, sysdate-365*1,
             'direccion de la Gaviota', null, sysdate-365*15, sysdate-365*1
        );


        dbms_output.put_line('*');
        dbms_output.put_line('Creaci�n de vista con sql din�mico');        
        dbms_output.put_line('*');        
        
        dinamico;

        -- ejemplo con sql din�mico y un cursor impl�cito.
        
        dbms_output.put_line('*');
        dbms_output.put_line('Select din�mico...');
        dbms_output.put_line('*');
        
     execute immediate
        'begin '
     || ' for r in (select * from direcciones_activas) loop '
     || '   dbms_output.put_line(  '
     || '    ''Nombre:''      || r.nombre                 || '
     || '    '', Direcci�n:'' || substr(r.direccion,1,20) || '
     || '    '', Tel�fono:''  || r.telefono);                '
     || ' end loop;                                          '
     || 'end;                                                 ';


        dbms_output.put_line('*');
        dbms_output.put_line('Ya terminamos las pruebas!');
        dbms_output.put_line('********');
        
   end probar;
    
   procedure limpiar is
   begin
      delete from contactos
         where entidad_id in (select id 
                              from  entidades
                              where nombre in ('Albatros','Buitre','Cormor�n',
                                               'Dodo','Faisan','Gaviota')
                            );
                            
      delete from entidades
        where nombre in ('Albatros','Buitre','Cormor�n',
                         'Dodo','Faisan','Gaviota');
   
   end limpiar;
   
BEGIN
   kk_hora_instanciado := to_char(sysdate, 'dd-mon-yyyy hh24:mi:ss');
end pq10;

/
