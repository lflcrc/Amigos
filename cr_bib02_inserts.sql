alter session set nls_date_format='dd/mm/yyyy';
-- primero se alimenta el cat�logo de roles que una persona desempe�a en una 
-- edici�n. 
insert into quien_es values (1,'Autor');
insert into quien_es values (2,'Compositor');
insert into quien_es values (3,'Int�rprete');
insert into quien_es values (4,'Int�rprete Invitado');
insert into quien_es values (5,'Ingeniero de sonido');
insert into quien_es values (6,'Productor');
insert into quien_es values (7,'Editor');
insert into quien_es values (8,'Fot�grafo');
insert into quien_es values (9,'Dibujante');
insert into quien_es values (10,'Pintor');
commit;

-- Se crean varias entidades.
insert into entidades values (1,'Anouar Brahen',null,null);
insert into entidades values (3,'Bill Bruford',null,null);
insert into entidades values (5,'Collen Mculloch',null,null);
insert into entidades values (7,'Dinorah Bolandi',null,null);
insert into entidades values (9,'Eberhard Weber',null,null);
insert into entidades values (11,'Farruquito',null,null);
insert into entidades values (13,'Gentle Giant',null,null);
insert into entidades values (100, 'Shawn Lane', null, null);
insert into entidades values (101, 'John Surman', null, null);
insert into entidades values (102, 'Dave Holland', null, null);
insert into entidades values (103, 'Tony Levin', null, null);
commit;

--OPUS
--delete from obras;
insert into obras values (1, 'Astrakan Cafe', '01/01/2001');
insert into obras values (2, 'Conte de L''incroyable Amour', '01/01/1992');
insert into obras values (3, 'Khomsa', '01/01/1995');
insert into obras values (4, 'Les Pas du Chat Noir', '01/01/2002');
insert into obras values (5, 'Thimar', '01/01/1998');
insert into obras values (6, 'Anderson Bruford Wakeman Howe', '01/01/1989');
insert into obras values (7, 'Bruford Levin Upper Extremities', '01/01/1999');
insert into obras values (8, 'Gradually going tornado', '01/01/1978');
insert into obras values (9, 'Powers of Ten', '01/01/1997');
commit;


-- editions
--delete from ediciones;
insert into ediciones values (1, 1, 'buen estado', 'CD', '01/01/2001', null);
insert into ediciones values (2, 2, 'regular estado', 'CD', '01/01/1992', null);
insert into ediciones values (3, 3, 'caja quebrada', 'CD', '01/01/1995', null);
insert into ediciones values (4, 4, 'p�simo estado, disco rayado', 'CD', '01/01/2002', null);
insert into ediciones values (5, 5, 'bien', 'CD', '01/01/1998', null);
insert into ediciones values (6, 6, 'conseguido en intercambio', 'CD', '01/01/1989', null);
insert into ediciones values (7, 7, 'regalo del d�a del padre', 'CD', '01/01/1999', null);
insert into ediciones values (8, 8, 'disponible para canje', 'CD', '01/01/1978', null);
insert into ediciones values (9, 9, 'remasterizado', 'CD', '01/01/1996', null);
insert into ediciones values (10, 9, 'edici�n japonesa', 'CD', '01/01/1998', null);
commit;


-- Edition details
--delete from detalle_edicion;

insert into detalle_edicion values  ( 1,     1,   2, null, null);
insert into detalle_edicion values  ( 1,     1,   3, null, null);
insert into detalle_edicion values  ( 1,     2,   2, null, null);
insert into detalle_edicion values  ( 1,     2,   3, null, null);
insert into detalle_edicion values  ( 1,     3,   2, null, null);
insert into detalle_edicion values  ( 1,     3,   3, null, null);
insert into detalle_edicion values  ( 1,     4,   2, null, null);
insert into detalle_edicion values  ( 1,     4,   3, null, null);
insert into detalle_edicion values  ( 1,     5,   2, null, null);
insert into detalle_edicion values  ( 1,     5,   3, null, null);
insert into detalle_edicion values  ( 101,   5,   3, null, null);
insert into detalle_edicion values  ( 102,   5,   3, null, null);
insert into detalle_edicion values  ( 3,     6,   3, null, null);
insert into detalle_edicion values  ( 3,     7,   2, null, null);
insert into detalle_edicion values  ( 3,     7,   3, null, null);
insert into detalle_edicion values  ( 103,   7,   2, null, null);
insert into detalle_edicion values  ( 103,   7,   3, null, null);
insert into detalle_edicion values  ( 103,   7,  10, null, null);
insert into detalle_edicion values  ( 3,     8,   2, null, null);
insert into detalle_edicion values  ( 3,     8,   3, null, null);
insert into detalle_edicion values  ( 100,   9,   2, null, null);
insert into detalle_edicion values  ( 100,   9,   3, null, null);
insert into detalle_edicion values  ( 100,  10,   2, null, null);
insert into detalle_edicion values  ( 100,  10,   3, null, null);
commit;

