CREATE OR REPLACE PACKAGE pq12 IS

  function sgte_id_contacto (p_entidad_id entidades.id%type) return number;

  function sgte_id (p_nom_tabla varchar2, p_nom_llave varchar2) return number;

  function  sgte_id_2col  
         (  p_nom_tabla   varchar2
         ,  p_nom_llave1  varchar2
         ,  p_val_llave1  number    -- valor de contexto para encontrar el m�ximo de llave2
         ,  p_nom_llave2  varchar2 ) 
                  return  number   ;
 
  
 function  sgte_id_3col   
         (  p_nom_tabla   varchar2
         ,  p_nom_llave1  varchar2
         ,  p_val_llave1  number    -- valor de contexto para encontrar el m�ximo de llave3
         ,  p_nom_llave2  varchar2
         ,  p_val_llave2  number    -- valor de contexto para encontrar el m�ximo de llave3
         ,  p_nom_llave3  varchar2 ) 
                  return  number   ; 
END;
/

CREATE OR REPLACE PACKAGE BODY pq12 IS

  -- Tiene datos si para una tabla p_tab, la columna p_col
  -- es llave primaria y no existen otras llaves primarias. 
	cursor  csr_chk_col_is_key_of
	     (  p_tab varchar2
	     ,  p_col varchar2)
  is	
  with lv as ( -- las columnas que son llave de la tabla indicada
     select  col.table_name       as  table_name
          ,  col.column_name      as  column_name
          ,  col.column_position  as  column_position
       from  all_constraints cn 
             inner join all_ind_columns col
             on (     cn.index_name = col.index_name
                 and  cn.owner      = col.index_owner )
      where  cn.constraint_type = 'P' -- es una llave primaria
        and  p_tab = col.table_name   -- es la tabla de inter�s 
   )
     select  1   
       from  lv
      where  p_col = column_name      -- la columna indicada es llave
		    and      1 = column_position
		    and  not exists               -- y no hay m�s llaves
		                    (select  2
		                       from  lv
										      where  column_position > 1);

  -- end of cursor csr_chk_col_is_key_of




  -- similar a csr_chk_col_is_key_of, excepto que este es para dos columnas.
  cursor  csr_chk_2col_are_key_of
	     (  p_tab  varchar2
	     ,  p_col1 varchar2
	     ,  p_col2 varchar2)
  is	
  with lv as ( -- las columnas que son llave de la tabla indicada
     select  col.table_name       as  table_name
          ,  col.column_name      as  column_name
          ,  col.column_position  as  column_position
       from  all_constraints cn 
             inner join all_ind_columns col
             on (     cn.index_name = col.index_name
                 and  cn.owner      = col.index_owner )
      where  cn.constraint_type = 'P' -- es una llave primaria
        and  p_tab = col.table_name   -- es la tabla de inter�s 
   )
     select  1   
       from  lv
      where  -- la columnas indicadas son llave en el orden indicado
		     (   ( p_col1 = column_name  and  1 = column_position )
		     or  ( p_col2 = column_name  and  2 = column_position )
		     )
		    and  not exists               -- y no hay m�s llaves
		                    (select  2
		                       from  lv
										      where  column_position > 2);

  -- end of cursor csr_chk_2col_are_key_of


  -- similar a csr_chk_col_is_key_of, excepto que este es para tres columnas.
  cursor  csr_chk_3col_are_key_of
	     (  p_tab  varchar2
	     ,  p_col1 varchar2
	     ,  p_col2 varchar2
	     ,  p_col3 varchar2)
  is	
  with lv as ( -- las columnas que son llave de la tabla indicada
     select  col.table_name       as  table_name
          ,  col.column_name      as  column_name
          ,  col.column_position  as  column_position
       from  all_constraints cn 
             inner join all_ind_columns col
             on (     cn.index_name = col.index_name
                 and  cn.owner      = col.index_owner )
      where  cn.constraint_type = 'P' -- es una llave primaria
        and  p_tab = col.table_name   -- es la tabla de inter�s 
   )
     select  1   -- este puede ser cualquier valor distinto de nulo.
       from  lv
      where  -- la columnas indicadas son llave en el orden indicado
		     (   ( p_col1 = column_name  and  1 = column_position )
		     or  ( p_col2 = column_name  and  2 = column_position )
		     or  ( p_col3 = column_name  and  3 = column_position )
		     )
		    and  not exists               -- y no hay m�s llaves
		                    (select  2  -- este puede ser cualquier valor distinto de nulo.
		                       from  lv
										      where  column_position > 3);

  -- end of cursor csr_chk_2col_are_key_of


  function  sgte_id_contacto 
         (  p_entidad_id  entidades.id%type )
                  return  number            
  is
     rslt contactos.id%TYPE;
  begin
  	select nvl( max(id), 0) + 1
  	into   rslt
  	from   contactos c
  	where  c.entidad_id = p_entidad_id;
  	return rslt;
  exception when others then
  	rslt := 0;
  	return rslt;
  end sgte_id_contacto;
  
  
  function  sgte_id 
         (  p_nom_tabla  varchar2
         ,  p_nom_llave  varchar2 ) 
                 return  number   
  is
     rslt  pls_integer := 0;       -- si continua en cero es un error.
     tmp   pls_integer := 0;
     found boolean     := false;
  begin
  	-- se utiliza el cursor para verificar que en la tabla p_nom_tabla,
  	-- p_nom_llave sea una llave y que no existan m�s llaves.
  	open  csr_chk_col_is_key_of( upper(p_nom_tabla) , upper(p_nom_llave) );
  	fetch csr_chk_col_is_key_of into rslt;
  	found := csr_chk_col_is_key_of%found;   
  	-- preservamos la respuesta. notfound significa que p_nom_llave no es una llave.
    close csr_chk_col_is_key_of;
  			 
	  if  not found then
	  	  raise_application_error(-20000, 'Este usuario no conoce una tabla "'|| p_nom_tabla ||
                                  		  '" que contenga una llave "' || p_nom_llave || '".');
  	end if;
  	
		-- �C�mo funciona bajo concurrencia?
    execute immediate 
  	   'select nvl( max(' || p_nom_llave || '), 0 ) + 1 from ' || p_nom_tabla into rslt;
  
 	
  	if  rslt = 0 then
  		  raise_application_error(-20000, 'No pudo obtener la siguiente llave para la tabla "'
  		                                   || p_nom_tabla ||'".');
    end if;
 
  	return rslt;
  	
  end sgte_id;
 
 
  function  sgte_id_2col   --  pudo haber sido una versi�n con sobrecarga de sgte_id 
         (  p_nom_tabla   varchar2
         ,  p_nom_llave1  varchar2
         ,  p_val_llave1  number    -- una valor de contexto para encontrar el m�ximo de llave2
         ,  p_nom_llave2  varchar2 ) 
                  return  number   
  is
     rslt  pls_integer := 0;       -- si continua en cero es un error.
     tmp   pls_integer := 0;
     found boolean     := false;
  begin
  	-- se utiliza el cursor para verificar que en la tabla p_nom_tabla,
  	-- p_nom_llave sea una llave y que no existan m�s llaves.
  	open  csr_chk_2col_are_key_of
  	      ( upper(p_nom_tabla) , upper(p_nom_llave1) , upper(p_nom_llave2) );
  	fetch csr_chk_2col_are_key_of into rslt;
  	found := csr_chk_2col_are_key_of%found;   
  	-- preservamos la respuesta. notfound significa que p_nom_llave no es una llave.
    close csr_chk_2col_are_key_of;
  			 
	  if  not found then
	  	  raise_application_error
	  	    (-20000, 'Este usuario no conoce una tabla "'|| p_nom_tabla ||
    		  '" que contenga las llaves "' || p_nom_llave1 || '" y "' || p_nom_llave2 || '".');
  	end if;
  	
		-- �C�mo funciona bajo concurrencia?
    execute immediate 
  	   'select nvl( max(' || p_nom_llave2 || '), 0 ) + 1 ' 
  	   || ' from '  || p_nom_tabla 
  	   || ' where ' || p_nom_llave1 || ' = ' || p_val_llave1
  	   into rslt;
  
 	
  	if  rslt = 0 then
  		  raise_application_error(-20000, 'No pudo obtener la siguiente llave para la tabla "'
  		                                   || p_nom_tabla ||'".');
    end if;
 
  	return rslt;
  	
  end sgte_id_2col;
  
  
  
 function  sgte_id_3col   --  pudo haber sido una versi�n con sobrecarga de sgte_id 
         (  p_nom_tabla   varchar2
         ,  p_nom_llave1  varchar2
         ,  p_val_llave1  number    -- una valor de contexto para encontrar el m�ximo de llave2
         ,  p_nom_llave2  varchar2
         ,  p_val_llave2  number
         ,  p_nom_llave3  varchar2 ) 
                  return  number   
  is
     rslt  pls_integer := 0;       -- si continua en cero es un error.
     tmp   pls_integer := 0;
     found boolean     := false;
  begin
  	-- se utiliza el cursor para verificar que en la tabla p_nom_tabla,
  	-- p_nom_llave sea una llave y que no existan m�s llaves.
  	open  csr_chk_3col_are_key_of
  	      ( upper(p_nom_tabla) 
  	      , upper(p_nom_llave1)
  	      , upper(p_nom_llave2)
  	      , upper(p_nom_llave3) );
  	fetch csr_chk_3col_are_key_of into rslt;
  	found := csr_chk_3col_are_key_of%found;   
  	-- preservamos la respuesta.
  	-- notfound significa que algo anda mal.
    close csr_chk_3col_are_key_of;
  			 
	  if  not found then
	  	  raise_application_error
	  	    (-20000, 'Este usuario no conoce una tabla "'|| p_nom_tabla ||
    		  '" que contenga las llaves "'  || p_nom_llave1 || 
    		  '", "' || p_nom_llave2 || '" y "' || p_nom_llave3 || '".');
  	end if;
  	
		-- �C�mo funciona bajo concurrencia?
    execute immediate 
  	   'select nvl( max(' || p_nom_llave3 || '), 0 ) + 1 ' 
  	   || ' from '  || p_nom_tabla 
  	   || ' where ' || p_nom_llave1 || ' = ' || p_val_llave1
  	   || ' and   ' || p_nom_llave2 || ' = ' || p_val_llave2
  	   into rslt;
  
 	
  	if  rslt = 0 then
  		  raise_application_error(-20000, 'No pudo obtener la siguiente llave para la tabla "'
  		                                   || p_nom_tabla ||'".');
    end if;
 
  	return rslt;
  	
  end sgte_id_3col;
  
  
END;
/
