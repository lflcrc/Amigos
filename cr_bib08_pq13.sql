
create or replace type  TString is table of varchar2(32000);
/
show err

create or replace package pq13 as 


  type  rec_prueba is record 
        (
          n1 number(10)
        , h1 varchar2(10)
        );

  procedure set    (valor    in  number);

  procedure get    (valor    out number);

  procedure getrec (x out rec_prueba);

  procedure duplicar0p;
  
  procedure duplicar1p (resultado in out number);

  procedure sumar2p (resultado in out number,
                     op1       in     number);
                
  procedure sumar3p (resultado in out number,
                     op1       in     number,
                     op2       in     number);

  procedure hileras (csr out sys_refcursor);

end pq13;
/

show err


create or replace package body pq13 as

  numero_oculto number := -1;

 
  procedure set    (valor    in  number) as
  begin
     numero_oculto := valor;
  end set;


  procedure get    (valor    out number) as
  begin
    valor := numero_oculto;
  end get;


  procedure getrec (x out rec_prueba) is
  begin
      x.n1 := 10;
      x.h1 := '0987654321';
  end getrec;


  procedure duplicar0p as
  begin
    numero_oculto := numero_oculto + numero_oculto; 
  end duplicar0p;


  procedure duplicar1p (resultado in out number) as
  begin
    resultado := resultado + resultado;
  end duplicar1p;


  procedure sumar2p (resultado in out number,
                     op1       in     number) as
  begin
    resultado := resultado + op1;
  end sumar2p;
 

  procedure sumar3p (resultado in out number,
                     op1       in     number,
                     op2       in     number) as
  begin
    resultado := op1 + op2;
  end sumar3p;


  procedure hileras (csr out sys_refcursor) as
     fechas TString := TString('uno', 'dos', 'tres');
  begin
     open csr for select * from table(fechas);
  end hileras;
  

end pq13;
/

show err
