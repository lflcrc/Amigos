select 'drop table ' || table_name || ' cascade constraints;' e
 from dba_tables t
       inner join dba_objects o on t.table_name = o.object_name
  where created  > sysdate -1;

  -- select object_name, object_type from dba_objects where created  > sysdate -1;
