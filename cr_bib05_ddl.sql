
drop view prestables;

create view prestables (obra, formato, estado, notas_edicion, propietario, 
                        obra_id, edicion_id, copia_id, entidad_id) 
as
select      
    o.nombre   as obra
  , ed.formato as formato
  , c.estado   as estado
  , ed.notas   as notas_edicion
  , ent.nombre as propietario
  , o.id       as obras_id
  , ed.id      as edicion_id
  , c.id       as copias_id
  , ent.id     as entidades_id
from
   copias c 
        inner join entidades ent on c.propietario_id = ent.id
        inner join ediciones ed  on c.edicion_id     = ed.id
        inner join obras     o   on ed.obra_id       = o.id;


-- Las entidades disponibles.

create or replace package pq11 as

    procedure registrar_propietarios;

    procedure registrar_copias;

    procedure remover_copias_y_propietarios;

    cursor propietarios_prueba              -- para facilitar la generación de valores de prueba
    is
        select  id,                         -- identificador de la persona
                ascii(substr(nombre,1,1)) v -- un número basado en la primer letra del nombre
          from  entidades
         where  nombre in ( 'Paquita', 'Mercedes', 'Yadira',
                            'Ana Cristina', 'Octavio', 'Federico')
           for  update;
    

    cursor copias_con_propietario
    is 
        select      
            c.estado   as estado
          , ent.nombre as propietario
          , c.id       as copias_id
          , ent.id     as entidades_id
        from
           copias c 
                inner join entidades ent on c.propietario_id = ent.id;


    cursor ediciones_disponibles
    is
        select  ed.id, o.nombre
        from    ediciones  ed inner join obras o on ed.obra_id = o.id;


    cursor ediciones_filtradas (x entidades.id%TYPE, y ediciones.id%TYPE)
    is
        select  id
          from  ediciones 
         where  mod(x, y)   = id;

end pq11;
/


create or replace package body pq11 as


procedure registrar_propietarios
is
    siguiente_id pls_integer;  -- retiene el máximo número de identificador.

begin

    select    nvl(max(id), 0) + 1-- obtiene el siguiente identificador a utilizar
        into  siguiente_id
        from  entidades;

    insert into entidades (id,nombre) values (siguiente_id, 'Paquita'); 
    siguiente_id := siguiente_id + 1; 
    insert into entidades (id,nombre) values (siguiente_id, 'Mercedes');
    siguiente_id := siguiente_id + 1; 
    insert into entidades (id,nombre) values (siguiente_id, 'Ana Cristina');
    siguiente_id := siguiente_id + 1; 
    insert into entidades (id,nombre) values (siguiente_id, 'Yadira');
    siguiente_id := siguiente_id + 1; 
    insert into entidades (id,nombre) values (siguiente_id, 'Octavio');
    siguiente_id := siguiente_id + 1; 
    insert into entidades (id,nombre) values (siguiente_id, 'Federico');

end registrar_propietarios;



procedure registrar_copias
is

    v_entidad_id     entidades.id%TYPE;   -- no se usa.

    v_max_ed_id      ediciones.id%TYPE   ;
    
    v_copia_id       copias.id%TYPE      ;
    
begin

   select  nvl(max(id), 0)       -- para tener un valor máximo.
     into  v_max_ed_id
     from  ediciones;
   
   select  nvl(max(id), 0) + 1   -- para generar el próximo id de copias.
     into  v_copia_id
     from  copias;
   

   registrar_propietarios;


   for  prop in  pq11.propietarios_prueba loop 

        --dbms_output.put_line(prop.id || ' ' || to_char(sysdate - prop.v)); 

        for e in ediciones_filtradas(prop.id, v_max_ed_id) loop
                
            insert 
                into    copias (id, edicion_id, propietario_id, fecha_adquirido)
                values  (
                     v_copia_id       -- id
                   , e.id             -- edicion_id
                   , prop.id          -- propietario_id
                   , sysdate - prop.v -- fecha_adquirido
             );

            v_copia_id := v_copia_id + 1;

        end loop;

    end loop;

   

end registrar_copias;
    

procedure remover_copias_y_propietarios
is
begin
    for p in propietarios_prueba loop

        delete
            from  copias
            where  propietario_id = p.id;
    
        delete
            from  entidades
            where  current of propietarios_prueba;
   
    end loop;

end remover_copias_y_propietarios;


end pq11;
/
