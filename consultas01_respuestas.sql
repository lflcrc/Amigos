-- q1
select id, nombre from entidades;

/*
        ID NOMBRE
---------- ---------------------------------
         1 Anouar Brahen
         3 Bill Bruford
         5 Collen Mculloch
         7 Dinorah Bolandi
         9 Eberhard Weber
        11 Farruquito
        13 Gentle Giant
       100 Shawn Lane
       101 John Surman
       102 Dave Holland
       103 Tony Levin
*/

--q2--
select count(*) from entidades where id > 5;

/*
  COUNT(*)
----------
         8
*/



--q3--
  select  nombre, count(*) cantidad
    from  obras, ediciones ed
   where  obras.id = obra_id
group by  nombre
  having  count(*) > 1;
/*

NOMBRE                           CANTIDAD
------------------------------ ----------
Powers of Ten                           2


*/



--q4--

  select  
            ent.nombre                  as interprete
          , obras.nombre                as obra
          , ed.fecha_ini_disponibilidad as lanzada
    from
            obras          
          , ediciones       ed
          , detalle_edicion det
          , quien_es        q
          , entidades       ent
  where
               obras.id = ed.obra_id
           and    ed.id = det.edicion_id
           and     q.id = det.quien_es_id
           and   ent.id = det.entidad_id   
           and  q.descripcion like ('Int�rprete%')
order by
           interprete, obra;
           
/*

INTERPRETE                     OBRA                                     LANZADA
------------------------------ ---------------------------------------- -------------------
Anouar Brahen                  Astrakan Cafe                            2001-01-01 00:00:00
Anouar Brahen                  Conte de L'incroyable Amour              1992-01-01 00:00:00
Anouar Brahen                  Khomsa                                   1995-01-01 00:00:00
Anouar Brahen                  Les Pas du Chat Noir                     2002-01-01 00:00:00
Anouar Brahen                  Thimar                                   1998-01-01 00:00:00
Bill Bruford                   Anderson Bruford Wakeman Howe            1989-01-01 00:00:00
Bill Bruford                   Bruford Levin Upper Extremities          1999-01-01 00:00:00
Bill Bruford                   Gradually going tornado                  1978-01-01 00:00:00
Dave Holland                   Thimar                                   1998-01-01 00:00:00
John Surman                    Thimar                                   1998-01-01 00:00:00
Shawn Lane                     Powers of Ten                            1996-01-01 00:00:00
Shawn Lane                     Powers of Ten                            1998-01-01 00:00:00
Tony Levin                     Bruford Levin Upper Extremities          1999-01-01 00:00:00

*/

--q5

-- version 1

select     
            obras.nombre as obra
          , q1.cantidad
  from
            obras
          , ediciones ed
          , (
                  select  
                            det.edicion_id 
                          , count(*)        as cantidad
                    from
                            detalle_edicion   det
                          , quien_es          q
                   where
                                det.quien_es_id = q.id
                           and  q.descripcion like ('Int�rprete%')
                group by  det.edicion_id
             ) q1
  where
                obras.id = ed.obra_id
           and     ed.id = q1.edicion_id
           and    q1.cantidad > 1;


-- version 2

select
          obras.nombre  as obra
        , count(*)      as cantidad
  from 
          obras
          inner join  ediciones       ed   on  obras.id = ed.obra_id
          inner join  detalle_edicion det  on     ed.id = det.edicion_id
          inner join  quien_es        q    on      q.id = det.quien_es_id
  where
          q.descripcion like ('Int�rprete%')
          -- en el group by es importante det.edicion_id
          -- porque filtra adicionalmente por edici�n.
group by  obras.nombre, det.edicion_id 
  having  count(*) > 1
order by  obras.nombre;

/* 
--=

              obra               | cantidad
---------------------------------+----------
 Thimar                          |        3
 Bruford Levin Upper Extremities |        2

*/

