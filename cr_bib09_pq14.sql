create or replace package pq14 IS

type tcoll_ids is table of varchar2(100) index by binary_integer;

type tcoll_dict is table of varchar2(100) index by varchar2(100);

type triplet is record
(    first_  varchar2(100)
,    second_ varchar2(100)
,    third_  varchar2(100)
);

type tcoll_triplets is table of triplet index by pls_integer;

ids       tcoll_ids;
dict      tcoll_dict;
triplets  tcoll_triplets;

function  mk_triplet 
       (           f varchar2
       ,           s varchar2
       ,           t varchar2
       )      return triplet;

function  get_ids      return tcoll_ids      ;
function  get_dict     return tcoll_dict     ;
function  get_triplets return tcoll_triplets ;
procedure show_status;

end pq14;
/


create or replace package body pq14 is

function  mk_triplet 
       (           f varchar2
       ,           s varchar2
       ,           t varchar2
       )      return triplet
      is
                   r triplet;
begin
   r.first_  := f;
   r.second_ := s;
   r.third_  := t;
   return r;
end mk_triplet;


function get_ids      return tcoll_ids      is begin return ids;      end;

function get_dict     return tcoll_dict     is begin return dict;     end;

function get_triplets return tcoll_triplets is begin return triplets; end;

procedure dump_triplets is
begin
   dbms_output.put_line('First   Second   Third');
   for i in triplets.first..triplets.last
   loop
        dbms_output.put_line
        (   triplets(i).first_  || '    ' 
        ||  triplets(i).second_ || '    ' 
        ||  triplets(i).third_  || '    ' 
        );
   end loop;
end dump_triplets;


procedure show_status is
begin
   dbms_output.put_line( 'Inhabitants:' );
   dbms_output.put_line( 'ids:' || ids.count );
   dbms_output.put_line( 'dict:' || dict.count );
   dbms_output.put_line( 'triples:' || triplets.count );
   dump_triplets;
end;


procedure init is
begin
       ids(1) := 'a';
       ids(2) := 'b';
       ids(3) := 'c';
       ids(4) := 'd';
    dict('a') := '1';
    dict('b') := '2';
    dict('c') := '3';
    dict('d') := '4';
  triplets(1) := mk_triplet(1, 'a', 'A');
  triplets(2) := mk_triplet(2, 'b', 'B');
  triplets(3) := mk_triplet(3, 'c', 'C');
  triplets(4) := mk_triplet(4, 'd', 'D');
end init;


begin 
  init; 
end pq14;
/

show err

prompt @/Users/lfonseca/Dropbox/all/db/amigos/cr_bib09_pq14.sql
